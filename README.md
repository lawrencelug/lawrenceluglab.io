# Setup steps
```
hugo new site lawrencelug.gitlab.io -f=yaml
cd lawrencelug.gitlab.io
git config --global init.defaultBranch main
git init
git submodule add https://github.com/halogenica/beautifulhugo.git themes/beautifulhugo
cp -r themes/beautifulhugo/exampleSite/* . -iv

git add .
git commit -m "Initial commit with beautifulhugo theme"
git remote add origin https://gitlab.com/websites694015/lawrencelug.gitlab.io.git
git push -u origin main
```
