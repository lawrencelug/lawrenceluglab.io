---
title: Meetup info
subtitle: Come join us!
comments: false
---

We meet on the third Saturday each month at the Lawrence Public Library.  We're going to try meeting room A going forward so we can give a remote option.  All experience levels are welcome to ask questions, get help, present a topic, request a topic to be presented, etc.  Please reach out via the IRC chat if there are any questions.

Details:
  - Time: 3pm the third Saturday of each month
  - In person: [Lawrence Public Library](https://lplks.org/visit/) room A
  - Online: https://meet.jit.si/LawrenceLUG
  - Add to calendar: [Download ICS](https://calendar.google.com/calendar/ical/kansaslug%40gmail.com/public/basic.ics) or [Google calendar](https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=MXUxdW9xN2poaDYzZmdmM2J0cDMydnRhNHNfMjAyMzA4MTlUMjAwMDAwWiBrYW5zYXNsdWdAbQ&amp;tmsrc=kansaslug%40gmail.com&amp;scp=ALL")
