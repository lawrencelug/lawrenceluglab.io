---
title: "Meeting Notes - 20230715"
date: 2023-07-15T23:43:02-05:00
draft: false
---

# Lawrence Linux User Group Meeting Notes - Saturday, July 15 2023

## Attendees
- Matt L.
- Nick A.

## Added a calendar for the LUG meetings

Linked to [ical](https://calendar.google.com/calendar/ical/kansaslug%40gmail.com/public/basic.ics) on the website [meetup page](https://lawrencelug.gitlab.io/page/meetup/).

## Missouri and Kansas Cyber Alliance Network

A relatively new meetup group that promotes free expression, security, privacy, creativity, and access to knowledge in the Sunflower and Show Me states.

Matt attended the most recent meeting on 2023-07-09[fn:1].

Attendance is better when held in Lawrence so the group plans to hold future meetups at the Lawrence Public Library for the foreseeable future.

https://www.mokancan.org

## Redhat

We chatted about the recent dust-up with respect to their changes to SRPM (source) availability[fn:2][fn:3]. How much has/does Red Hat benefit from the downstream rebuilds (CentOS Classic, Alma, Rocky, Whitebox, Scientific Linux, etc ...)? SUSE also spawning a Red Hat clone[fn:4].

This lead into a discussion about brain drain at Red Hat and a short story about how Rocky Linux has connections to Lawrence KS.

Rocky Linux was named for Rocky McGaugh[fn:5]. Rocky McGaugh was one of the CentOS project founders. Rocky was CTO of Atipa Technologies, a subdivision of Microtek Computers in Lawrence built and delivered the worlds 11th fastest supercomputer to LSU in 2002[fn:6] and went on to found TeamHPC[fn:7].

## NixOS

Lot's of chatter about it. Nick finds it pretty interesting, but wildly different.

## Social Media

Nick has been enjoying watching the various Social Media networks implode.

- Twitter has limited post views and login is required to view tweets
- Reddit API changes causing all kinds of shenanagins like /r/PICS only allowing pictures that include Jon Oliver
- Facebook launched Threads (twitterlike)

Nick has been enjoying the Fediverse (Mastadon, Lemmy). No ads, generally respectful people. Hopefully ActivityPub isn't killed off by corporate interests who want to embrace and extend (like google did to XMPP). Threads does or will use ActivityPub, remains to be seen who will federate with them.

## Testing out conference room A

We tested out the equipment in Conference Room A. There is a nice Logi Bar and TV that could be useful for sharing and having hybrid meetings. The MOKANCAN meeting was held there. It worked fine with Matt and Nick's laptops once we plugged in the USB cable.

## Footnotes

[fn:1] https://www.mokancan.org/mokancanorg/we-had-a-great-mokancan-meetup-at-lawrence-public-library-today

[fn:2] https://www.redhat.com/en/blog/furthering-evolution-centos-stream

[fn:3] https://www.redhat.com/en/blog/red-hats-commitment-open-source-response-gitcentosorg-changes

[fn:4] https://www.suse.com/news/SUSE-Preserves-Choice-in-Enterprise-Linux/

[fn:5] https://www.theregister.com/2020/12/10/rocky_linux/

[fn:6] https://www.phys.lsu.edu/faculty/tohline/capital/beowulf.html

[fn:7] https://www.linux.com/news/teamhpc-focuses-open-source/
